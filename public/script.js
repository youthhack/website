document.addEventListener('DOMContentLoaded', function() {
    var fontFamilies = [
        'Oxanium',
        'Bangers',
        'Monoton',
        'Press Start 2P',
        'Frijole',
        'Faster One',
        'Wallpoet',
        'Bungee Shade',
        'Codystar',
        'Chango',
        'Plaster',
        'Stalinist One',
    ];
    var fontFamily = fontFamilies[Math.floor(Math.random() * fontFamilies.length)];
    console.log(fontFamily);
    document.querySelector('header h1').style.fontFamily = '\'' + fontFamily + '\', sans-serif';

    var backgroundColors = [
        '#FF0046',
        '#80263E',
        '#B700FF',
        '#662680',
        '#0601FF',
        '#282680',
        '#109709',
        '#298026',
        '#B3AF3B',
        '#666303',
        '#B36A36',
        '#662A00',
    ];
    var colorLeft = backgroundColors[Math.floor(Math.random() * backgroundColors.length)];
    var colorRight = colorLeft;
    while (colorLeft === colorRight) {
        colorRight = backgroundColors[Math.floor(Math.random() * backgroundColors.length)];
    }
    document.body.style.backgroundImage = 'linear-gradient(40deg, '+colorLeft+' 0%, '+colorRight+' 100%)';

    // Nächsten Termin berechnen
    var firstDate = new Date('2022-09-13T18:00:00');
    var nextDate = new Date();

    if (nextDate.getHours() > firstDate.getHours()) {
        nextDate.setDate(nextDate.getDate()+1);
    }

    var getNextDateForWeekday = function(date, weekDay) {
        while (date.getDay() !== weekDay) {
            date.setDate(date.getDate()+1);
        }

        return date;
    };

    nextDate = getNextDateForWeekday(nextDate, firstDate.getDay());

    var oneDay = 24 * 60 * 60 * 1000;
    var diffDays = Math.abs((firstDate - nextDate) / oneDay);

    // Wenn wir nicht in der zweiten Woche sind, eine Woche addieren und nochmal versuchen
    if (Math.round(diffDays) % 14 !== 0) {
        nextDate.setDate(nextDate.getDate()+7);
        nextDate = nextDate = getNextDateForWeekday(nextDate, firstDate.getDay());
    }

    console.log(nextDate);

    var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

    document.querySelector('.next_date').innerHTML = '<h3>Nächster Termin:</h3><p class="date">'+nextDate.toLocaleDateString('de-DE', options)+'</p>';
}, false);
